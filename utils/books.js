const fs = require ('fs');

const loadBooks = () => {
    const fileBuffer = fs.readFileSync('data/books.json', 'utf-8')
    const book = JSON.parse(fileBuffer);
    return book;
}

module.exports = { loadBooks };