// import modul
const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const { loadBooks } = require('./utils/books.js')

const app = express();
const port = 8000;

// template engines
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(expressLayouts);

app.get('/', (req, res) => {
    // import loadbooks to get list books
    const books = loadBooks();
    res.status(200).render('index', {
        layout: 'layouts/main-layout',
        title: 'Home',
        books
    });
});

app.get('/register', (req, res) => {
    res.status(200).render('register', {
        layout: 'layouts/main-layout',
        title: 'Register'
    });
});

app.get('/buy', (req, res) => {
    res.status(200).render('buy', {
        layout: 'layouts/main-layout',
        title: 'Buy'
    });
});

//if the request page is not recognized
app.use((req, res) => {
    res.status(400).send('404 not found');
});

// to run port 8000
app.listen(port, () => {
    console.log(`Example app listening on port ${port}`);
})